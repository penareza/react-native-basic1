
import React from 'react';
import {ScrollView,  View} from 'react-native';
import SampleComponent from './pages/SampleComponent';
import FlexBox from './pages/FlexBox';
import Positionreactnative from './pages/Position';
import StyleComponent from './pages/StyleComponent';
const App = () => {
  return (
    
    <View>
      <ScrollView>
      <SampleComponent/>
      <StyleComponent/>
      <FlexBox />
      <Positionreactnative/>
      </ScrollView>
    </View>
  );
}




export default App;