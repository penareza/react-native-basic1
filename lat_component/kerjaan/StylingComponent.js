import React, { Component }  from 'react';
import {Image, ScrollView, StyleSheet, Text, TextInput, View} from 'react-native';
import macbook from '../mcpro.jpg';
const StylingComponent = () => {
    return (
      <View>
      <Text style={styles.text}>Styling</Text>
      <View style={{
        width:100, 
        height:100, 
        backgroundColor:'#eaeaea', 
        borderWidth:2, 
        borderColor:'red',
        marginTop:20,
        marginLeft:20,
        
        }}/>
  
        <View style={{
          padding: 12, 
          backgroundColor:'#f2f2f2', 
          width:212,
          borderRadius:8,
          marginTop:10,
          marginLeft:20,
          }}>
          <Image source={macbook} style=
          {{
            width:188, 
            height:107, 
            borderRadius:8,
            }}
            />
  
            <Text style={{
              fontSize:14,
              fontWeight:"bold",
              marginTop:16,
            }}>
              New Macbook Pro </Text>
              <Text style={{fontSize:12, fontWeight:"bold", color:'salmon', marginTop:14,}}>12.500.000</Text>
              <Text style={{fontWeight:"bold"}}>Jakarta Barat</Text>
              <View style={{
                backgroundColor:'lightgreen',
                marginTop:12, 
                borderRadius:25,
                paddingVertical:6,
                marginTop:20,
                }}>
                <Text style={{
                  textAlign:'center',
                  fontWeight:"600",
                  fontSize:14,
                  color:'white',
              }}>BELI</Text>
              </View>
        </View>
      </View>
    );
  }


  const styles = StyleSheet.create({
    text:{
      fontSize:18,
      fontWeight:"bold",
      color:'green',
      marginLeft:20,
      marginTop:40,
    }
  })

  export default StylingComponent;
  