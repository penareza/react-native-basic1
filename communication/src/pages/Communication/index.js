import React, {useState} from 'react'
import { StyleSheet, Text, View } from 'react-native'
import Keranjang from '../../components/keranjang'
import Produk from '../../components/produk'

const Communication = () => {
    const [totalProduk, settotalProduk] = useState(0);
    return (
        <View style={styles.container}>
            <Text style={styles.TextTittle}>Communication</Text>
            <Keranjang quantity={totalProduk}/>
            <Produk onButtonPress={() => settotalProduk(totalProduk + 1)}/>
           
        </View>
    )
}

export default Communication

const styles = StyleSheet.create({
    container: {
        padding:20,
    },

    TextTittle: {
        textAlign:'center',
    },

})
