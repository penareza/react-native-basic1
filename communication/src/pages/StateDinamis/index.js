import React, {useState} from 'react'
import { StyleSheet, Text, View, Button} from 'react-native'


const Counter = () => {
    const [angka, setAngka] = useState(0);
    return (
        <View>
            <Text style={styles.point}>{angka}</Text>
            <View style={{marginBottom:10,}}> 
                <Button title="Tambah" onPress={ () => setAngka(angka + 1)}
                style={styles.btn}/>
            </View>
            
            <View>
                <Button title="Kurang" onPress={ () => setAngka(angka - 1)}/>
            </View>
            
        </View>
    )
}




const StateDinamis = () => {
    return (
        <View style={styles.wrapper}>
            <Text style={styles.Texttitle}>
                State Dinamis
            </Text>
            <Counter/>
            
           
        </View>
    )
}

export default StateDinamis;

const styles = StyleSheet.create({
    wrapper:{
        padding:20,
    },

    Texttitle: {
        textAlign:'center',
    },

    point: {

        fontSize:50,
        textAlign:'center',
        marginBottom:10,
        
    }

})
