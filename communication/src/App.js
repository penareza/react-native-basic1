
import React from 'react';
import {ScrollView,  View} from 'react-native';
import SampleComponent from './pages/SampleComponent';
import FlexBox from './pages/FlexBox';
import Positionreactnative from './pages/Position';
import StyleComponent from './pages/StyleComponent';
import PropsDinamis from './pages/PropsDinamis';
import StateDinamis from './pages/StateDinamis';
import Communication from './pages/Communication';
const App = () => {
  return (
    
    <View>
      <ScrollView>
      {/* <SampleComponent/> */}
      {/* <StyleComponent/> */}
      {/* <FlexBox /> */}
      {/* <Positionreactnative/> */}
      {/* <PropsDinamis/> */}
      {/* <StateDinamis/> */}
      <Communication/>
      </ScrollView>
    </View>
  );
}

export default App;