import React from 'react'
import { StyleSheet, Text, View, Image } from 'react-native';
import cart from '../../assets/icon/s_chart.png';

const Keranjang = (props) => {
    return (
       <View style={{alignItems:'center'}}>
            <View style={styles.cartwrapper}>
            <Image source={cart} style={styles.iconCart} />
    <Text style={styles.notif}>{props.quantity}</Text>
            </View>
            <Text style={styles.tulisan}>Keranjang Belanja Anda</Text>
       </View>
        
         
    
    )
}

export default Keranjang

const styles = StyleSheet.create({
    cartwrapper:
    {
        borderWidth:1, 
        borderColor: '#95a5a6', 
        width:93, 
        height:93, 
        borderRadius:93 / 2,
        justifyContent:'center',
        alignItems:"center",
        position:"relative",
        marginTop:20,


    },

    iconCart:{
        width:67, height:65,

    },
    tulisan: 
    {
        fontSize:12, 
        fontWeight: "700", 
        marginTop:10,
    },
    notif:{
        fontSize:12,
        color:'white',
        backgroundColor:'#2ecc71',
        width:24,
        height:24,
        padding:4,
        borderRadius:24 /2,
        position:'absolute',
        top:0,
        right:0,


    }
})
