import React from 'react'
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import macbook from '../../assets/img/mcpro.jpg';

const Produk = (props) => {
    return (
        <View> 
        <View style={{
          padding: 12, 
          backgroundColor:'#f2f2f2', 
          width:212,
          borderRadius:8,
          marginTop:10,
          marginLeft:20,
          }}>
          <Image source={macbook} style=
          {{
            width:188, 
            height:107, 
            borderRadius:8,
            }}
            />
  
            <Text style={{
              fontSize:14,
              fontWeight:"bold",
              marginTop:16,
            }}>
              New Macbook Pro </Text>
              <Text style={{fontSize:12, fontWeight:"bold", color:'salmon', marginTop:14,}}>12.500.000</Text>
              <Text style={{fontWeight:"bold"}}>Jakarta Barat</Text>
              <View style={{
                backgroundColor:'lightgreen',
                marginTop:12, 
                borderRadius:25,
                paddingVertical:6,
                marginTop:20,
                }}>
                    <TouchableOpacity onPress={props.onButtonPress}>
                    <Text style={{
                        textAlign:'center',
                        fontWeight:"600",fontSize:14,color:'white'}}>BELI</Text>
                    </TouchableOpacity>
                
              </View>
        </View>
        </View>
    )
}

export default Produk

const styles = StyleSheet.create({})
