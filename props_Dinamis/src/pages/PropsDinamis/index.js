import React from 'react'
import { StyleSheet, Image, Text, View, ScrollView } from 'react-native'

const Story = (props) => {
    return(
        <View style={{alignItems:"center", marginRight:20}}>
        <Image source={{uri:props.gambar,
        }} 
        style={{width:70, height:70, borderRadius:70}}/>
        <Text style={{maxWidth:50, textAlign:'center'}}>{props.judul}</Text>
        </View>
      

    )
}

const PropsDinamis = () => {
    return (
        <View>
            <Text>Props dinamis</Text>
            <ScrollView horizontal>
            <View style={{flexDirection:"row"}}>
            <Story judul="Youtube Channel" gambar ='https://www.wikihow.com/images/thumb/f/f3/Start-a-Good-Story-Step-2.jpg/aid684286-v4-728px-Start-a-Good-Story-Step-2.jpg'/>
            <Story judul="Kelas Online" gambar ='https://www.wikihow.com/images/thumb/6/6c/Start-a-Good-Story-Step-8.jpg/aid684286-v4-728px-Start-a-Good-Story-Step-8.jpg'/>
            <Story judul="ibio Coding" gambar ='https://www.wikihow.com/images/thumb/2/2a/Start-a-Good-Story-Step-7.jpg/aid684286-v4-728px-Start-a-Good-Story-Step-7.jpg'/>
            <Story judul="Shoot" gambar ='https://www.wikihow.com/images/thumb/6/62/Start-a-Good-Story-Step-6.jpg/aid684286-v4-728px-Start-a-Good-Story-Step-6.jpg'/>
            <Story judul="Kelas Online" gambar ='https://www.wikihow.com/images/thumb/6/6c/Start-a-Good-Story-Step-8.jpg/aid684286-v4-728px-Start-a-Good-Story-Step-8.jpg'/>
            <Story judul="ibio Coding" gambar ='https://www.wikihow.com/images/thumb/2/2a/Start-a-Good-Story-Step-7.jpg/aid684286-v4-728px-Start-a-Good-Story-Step-7.jpg'/>
            <Story judul="Shoot" gambar ='https://www.wikihow.com/images/thumb/6/62/Start-a-Good-Story-Step-6.jpg/aid684286-v4-728px-Start-a-Good-Story-Step-6.jpg'/>

            </View>
            </ScrollView>
           
            
        </View>
    )
}

export default PropsDinamis

const styles = StyleSheet.create({})
