import React, { Component, useEffect, useState } from 'react'
import { Image, Text, View } from 'react-native';

// class FlexBox extends Component {
//     render() {
//         return (

//             <View>
//                 <View style={{
//                 flexDirection:"row", 
//                 backgroundColor:'#eaeaea',
//                 alignItems:'flex-start',
//                 justifyContent:'space-between',
//                 }}>
                 
//                 <View style={{backgroundColor:'#2980b9', width:50, height:50}}/>
//                 <View style={{backgroundColor:'#27ae60', width:50, height:50}}/>
//                 <View style={{backgroundColor:'#f1c40f', width:50, height:50}}/>
//                 <View style={{backgroundColor:'#e67e22', width:50, height:50}}/>
//             </View>
//             <View style={{flexDirection:'row',    justifyContent:'space-around'}}>
//             <Text>Beranda</Text>
//             <Text>Beranda</Text>
//             <Text>Beranda</Text>
//             <Text>Beranda</Text>
//             </View>

            
            
//             <View style={{flexDirection:'row', alignItems:"center", marginTop:20, marginLeft:10,}}>

//             <Image source= {{uri:'https://yt3.ggpht.com/a/AATXAJy5LLCs-DPIxCnBLN8vcAYGVf7b7Mw8lagUA5aK8w=s88-c-k-c0x00ffffff-no-rj'}}
//             style={{width:100, height:100, borderRadius:50, marginRight:14,}}/>
//             <View>
//                 <Text style={{fontSize:20, fontWeight:"bold"}}>Muammar Reza</Text>
//                 <Text>100 Ribu Subscribers</Text>
//             </View>
            
//             </View>
           
//             </View>
            

//         ) 
//     }
// }

const FlexBox = () => {
    const [Subscribe, setSubscribe] = useState(200);
    useEffect(() => {
        console.log("Did Mount");
        setTimeout(() => {
            setSubscribe(300);
        }, 2000);
      return () => {
        console.log('Did Update');
      };
    }, [Subscribe]);

    // useEffect(() => {
    //     return () => {
    //       //console.log('did Update');
    //       setTimeout(() => {
    //           setSubscribe(400);
    //       }, 4000);
    //     };
    //   }, [Subscribe]);


    return (

        <View>
            <View style={{
            flexDirection:"row", 
            backgroundColor:'#eaeaea',
            alignItems:'flex-start',
            justifyContent:'space-between',
            }}>
             
            <View style={{backgroundColor:'#2980b9', width:50, height:50}}/>
            <View style={{backgroundColor:'#27ae60', width:50, height:50}}/>
            <View style={{backgroundColor:'#f1c40f', width:50, height:50}}/>
            <View style={{backgroundColor:'#e67e22', width:50, height:50}}/>
        </View>
        <View style={{flexDirection:'row',    justifyContent:'space-around'}}>
        <Text>Beranda</Text>
        <Text>Beranda</Text>
        <Text>Beranda</Text>
        <Text>Beranda</Text>
        </View>

        
        
        <View style={{flexDirection:'row', alignItems:"center", marginTop:20, marginLeft:10,}}>

        <Image source= {{uri:'https://yt3.ggpht.com/a/AATXAJy5LLCs-DPIxCnBLN8vcAYGVf7b7Mw8lagUA5aK8w=s88-c-k-c0x00ffffff-no-rj'}}
        style={{width:100, height:100, borderRadius:50, marginRight:14,}}/>
        <View>
            <Text style={{fontSize:20, fontWeight:"bold"}}>Muammar Reza</Text>
        <Text>{Subscribe} Ribu Subscribers</Text>
        </View>
        
        </View>
       
        </View>
        

    ) 
}

export default FlexBox;
